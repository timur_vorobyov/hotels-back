import { StatusCodes } from "http-status-codes";
import { Request, Response } from 'express';
import { createHotel, deleteHotel, updateHotel, getHotel, getHotels, getHotelsCountByCity, getHotelsCountByType } from "./hotel.service";
import { CreateHotelBody } from "./hotel.schema";

export async function createHotelHandler(req: Request<{}, {}, CreateHotelBody>, res: Response) {
  try {
    await createHotel(req.body);

    return res.status(StatusCodes.CREATED).send("HOTEL_CREATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function updateHotelHandler(
  req: Request<{ id: number; }, {}, CreateHotelBody>,
  res: Response) {

  try {
    await updateHotel(req.params.id, req.body);
    return res.status(StatusCodes.OK).send("HOTEL_UPDATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function deleteHotelHandler(
  req: Request<{ id: number; }, {}, {}>,
  res: Response) {

  try {
    await deleteHotel(req.params.id);
    return res.status(StatusCodes.OK).send("HOTEL_DELETED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}


export async function getHotelHandler(
  req: Request<{ id: number; }, {}, {}>,
  res: Response) {

  try {
    const hotel = await getHotel(req.params.id);
    return res.status(StatusCodes.OK).send(hotel);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function getHotelsHandler(
  req: Request<{}, {}, {}>,
  res: Response) {

  try {
    const hotels = await getHotels();
    return res.status(StatusCodes.OK).send(hotels);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function countByCityHandler(
  req: Request<{}, {}, {}, { cities: string; }>,
  res: Response) {

  const { cities } = req.query;
  try {
    const list = await getHotelsCountByCity(cities);
    return res.status(StatusCodes.OK).send(list);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function countByTypeHandler(
  req: Request<{}, {}, {}>,
  res: Response) {

  try {
    const hotelCount = await getHotelsCountByType("hotel");
    const apartmentCount = await getHotelsCountByType("apartment");
    const resortCount = await getHotelsCountByType("resort");
    const villaCount = await getHotelsCountByType("villa");
    const cabinCount = await getHotelsCountByType("cabin");
    return res.status(StatusCodes.OK).send([
      { type: " hotel", count: hotelCount },
      { type: " apartment", count: apartmentCount },
      { type: " resort", count: resortCount },
      { type: " villa", count: villaCount },
      { type: " cabin", count: cabinCount },
    ]);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}