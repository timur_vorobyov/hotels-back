import { Room, RoomModal } from "./room.model";

export async function createRoom(room: Room) {
  return RoomModal.create(room);
}

export async function updateRoom(id: number, room: Room) {
  return RoomModal.findByIdAndUpdate(id, { $set: room });
}

export async function deleteRoom(id: number) {
  return RoomModal.findByIdAndDelete(id);
}

export async function getRoom(id: number) {
  return RoomModal.findById(id);
}

export async function getRooms() {
  return RoomModal.find();
}