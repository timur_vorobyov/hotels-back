import { Hotel, HotelModal } from "./hotel.model";

export async function createHotel(hotel: Hotel) {
  return HotelModal.create(hotel);
}

export async function updateHotel(id: number, hotel: Hotel) {
  return HotelModal.findByIdAndUpdate(id, { $set: hotel });
}

export async function deleteHotel(id: number) {
  return HotelModal.findByIdAndDelete(id);
}

export async function getHotel(id: number) {
  return HotelModal.findById(id);
}

export async function getHotels() {
  return HotelModal.find();
}

export async function updateHotelFieldPush(id: number, field: string, value: typeof Hotel) {
  return HotelModal.findByIdAndUpdate(id, {
    $push: { [field]: value }
  });
}

export async function updateHotelFieldPull(id: number, field: string, roomId: number) {
  return HotelModal.findByIdAndUpdate(id, {
    $pull: { [field]: roomId }
  });
}

export async function getHotelsCountByCity(cities: string) {
  const citiesArr = cities.split(",");
  return await Promise.all(citiesArr.map((city: string) => {
    return HotelModal.countDocuments({ city: city });
  }));
}

export async function getHotelsCountByType(type: string) {
  return await HotelModal.countDocuments({ type: type });
}