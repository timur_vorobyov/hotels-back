import express from "express";
import {
  createHotelHandler,
  deleteHotelHandler,
  updateHotelHandler,
  getHotelHandler,
  getHotelsHandler,
  countByTypeHandler,
  countByCityHandler
} from "./hotel.controller";

const router = express.Router();

router.post('/', createHotelHandler);
router.put('/:id', updateHotelHandler);
router.delete('/:id', deleteHotelHandler);
router.get('/find/:id', getHotelHandler);


router.get('/', getHotelsHandler);
router.get("/countByCity", countByCityHandler);
router.get("/countByType", countByTypeHandler);

export default router;