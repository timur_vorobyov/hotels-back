import express from "express";
import {
  createRoomHandler,
  deleteRoomHandler,
  updateRoomHandler,
  getRoomHandler
} from "./room.controller";

const router = express.Router();

router.post('/:hotelId', createRoomHandler);
router.put('/:id', updateRoomHandler);
router.delete('/:id/:hotelId', deleteRoomHandler);
router.get('/:id', getRoomHandler);
router.get('/', getRoomHandler);

export default router;