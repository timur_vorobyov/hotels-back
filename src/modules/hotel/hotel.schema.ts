import { array, boolean, number, object, string, TypeOf } from 'zod';

export const createHotelSchema = {
  body: object({
    name: string({
      required_error: "name is required",
    }),
    type: string({
      required_error: "type is required"
    }),
    city: string({
      required_error: "city is required"
    }),
    address: string({
      required_error: "address is required"
    }),
    distance: string(),
    photos: array(string()),
    desc: string(),
    rating: number()
      .min(0, "Rating must be at least 0")
      .max(5, "Rating should not be more than 5"),
    rooms: array(string()),
    chipestPrice: number()
      .min(0, "Chipest price must be at least 0")
      .max(5, "Chipest price should not be more than 5"),
    featured: boolean()
  })
};

export type CreateHotelBody = TypeOf<typeof createHotelSchema.body>;