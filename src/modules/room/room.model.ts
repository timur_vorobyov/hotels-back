import { getModelForClass, modelOptions, prop, Severity } from "@typegoose/typegoose";

@modelOptions({
  options: {
    allowMixed: Severity.ALLOW,
  },
})

export class Room {
  @prop({ required: true, unique: true })
  public title: string;

  @prop({ required: true, unique: true })
  public price: number;

  @prop({ required: true })
  public desc: string;

  @prop({ required: true })
  public maxPeople: number;

  @prop({ required: true })
  public roomNumbers: { number: number, unavailableDates: { type: Date[]; }; }[];

}

export const RoomModal = getModelForClass(Room, {
  schemaOptions: {
    timestamps: true
  }
}
);