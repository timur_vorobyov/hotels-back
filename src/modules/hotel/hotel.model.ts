import { getModelForClass, prop, modelOptions, Severity } from "@typegoose/typegoose";

@modelOptions({
  options: {
    allowMixed: Severity.ALLOW,
  },
})

export class Hotel {
  @prop({ required: true })
  public name: string;

  @prop({ required: true })
  public type: string;

  @prop({ required: true })
  public city: string;

  @prop({ required: true })
  public address: string;

  @prop({ required: true })
  public distance: string;

  @prop()
  public photos: string[];

  @prop({ required: true })
  public desc: string;

  @prop({ min: 0, max: 5 })
  public rating: number;

  @prop()
  public rooms: string[];

  @prop({ min: 0, max: 5 })
  public chipestPrice: number;

  @prop({ default: false })
  public featured: boolean;
}

export const HotelModal = getModelForClass(Hotel, {
  schemaOptions: {
    timestamps: true
  }
}
);