import { StatusCodes } from "http-status-codes";
import { Request, Response } from 'express';
import { createRoom, deleteRoom, getRoom, getRooms, updateRoom, } from "./room.service";
import { updateHotelFieldPull, updateHotelFieldPush } from "../hotel/hotel.service";
import { CreateRoomBody } from "./room.schema";


export async function createRoomHandler(
  req: Request<{ hotelId: number; }, {}, CreateRoomBody>,
  res: Response) {
  try {
    const hotelId = req.params.hotelId;
    const savedRoom = await createRoom(req.body);
    await updateHotelFieldPush(hotelId, 'rooms', savedRoom.id);

    return res.status(StatusCodes.CREATED).send("ROOM_CREATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function updateRoomHandler(
  req: Request<{ id: number; }, {}, CreateRoomBody>,
  res: Response) {

  try {
    await updateRoom(req.params.id, req.body);
    return res.status(StatusCodes.OK).send("ROOM_UPDATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function deleteRoomHandler(
  req: Request<{ id: number; hotelId: number; }, {}, {}>,
  res: Response) {

  try {
    await deleteRoom(req.params.id);
    const hotelId = req.params.hotelId;
    await updateHotelFieldPull(hotelId, 'rooms', req.params.id);
    return res.status(StatusCodes.OK).send("ROOM_DELETED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}


export async function getRoomHandler(
  req: Request<{ id: number; }, {}, {}>,
  res: Response) {

  try {
    const room = await getRoom(req.params.id);
    return res.status(StatusCodes.OK).send(room);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function getRoomsHandler(
  req: Request<{}, {}, {}>,
  res: Response) {

  try {
    const rooms = await getRooms();
    return res.status(StatusCodes.OK).send(rooms);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}