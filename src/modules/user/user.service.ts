import { User, UserModal } from "./user.model";

export async function createUser(user: Omit<User, "comparePassword">) {
  return UserModal.create(user);
}

export async function findUserByEmail(email: User['email']) {
  return UserModal.findOne({ email });
}