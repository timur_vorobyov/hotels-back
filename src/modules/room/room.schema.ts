import { array, date, number, object, string, TypeOf } from 'zod';


export const createRoomSchema = {
  body: object({
    title: string({
      required_error: "title is required",
    }),
    price: number({
      required_error: "price is required"
    }),
    desc: string({
      required_error: "desc is required"
    }),
    maxPeople: number({
      required_error: "maxPeople is required"
    }),
    roomNumbers: array(object({
      number: number(),
      unavailableDates: object({
        type: array(date())
      })
    }))
  })
};

export type CreateRoomBody = TypeOf<typeof createRoomSchema.body>;